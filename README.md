# Fiza 

## Theme

Fiza - a Twenty Twenty One child theme for WordPress.

## Prerequisites

### Sass

This project is build with the Dart Sass standalone executable. See instructions at [Dart Sass Github repository](https://github.com/sass/dart-sass). If you cannot or do not want to use it, you may want to install [an npm package](https://www.npmjs.com/package/sass) that contains compiled to JavaScript version `npm install --save-dev sass`. Be aware that JavaScript compiled version is much slower than standalone executable though.

### CI/CD

GitLab CI/CD pipeline requires setting variables for deployment stage. You may want to adjust `.gitlab-ci.yml` variables and/or set them in a project's `Setting - CI/CD - Variables` GitLab admin panel.

### Environment variables

Set variables in `.env`, `.gitlab-ci.yml` and adjust `.browser-sync-https.js` for VVV or XAMPP paths and `package.json` scripts for source and destination paths.

## License

Project is licensed under [GPLv2 or later](http://www.gnu.org/licenses/gpl-2.0.html).
