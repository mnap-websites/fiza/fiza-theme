=== Fiza ===
Contributors: mnap
Requires at least: 5.3
Tested up to: 5.8
Requires PHP: 7.3
Stable tag: 1.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

== Description ==

Fiza - a Twenty Twenty One child theme for WordPress.

== Installation ==

1. In your admin panel, go to Appearance -> Themes and click the 'Add New' button.
2. Type in Fiza in the search form and press the 'Enter' key on your keyboard.
3. Click on the 'Activate' button to use your new theme right away.
5. Navigate to Appearance > Customize in your admin panel and customize to taste.

== Privacy ==
Twenty Twenty-One uses LocalStorage to save the setting when Dark Mode support is turned on or off.
LocalStorage is necessary for the setting to work and is only used when a user clicks on the Dark Mode button.
No data is saved in the database or transferred.

== Changelog ==

= 1.0 =
* Released: TBD

Initial release

== Copyright ==

Fiza, 2021 mnap
Fiza is distributed under the terms of the GNU GPL.

Twenty Twenty-One WordPress Theme, 2020 WordPress.org
Twenty Twenty-One is distributed under the terms of the GNU GPL.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
	 but WITHOUT ANY WARRANTY; without even the implied warranty of
	 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	 GNU General Public License for more details.


	 Twenty Twenty-One is derived from Seedlet, (C) 2020 Automattic, Inc.

	 Twenty Twenty-One is also based on:

	 Twenty Nineteen. 2018-2020 WordPress.org
	 Twenty Nineteen is distributed under the terms of the GNU GPL v2 or later.

	 Twenty Seventeen. Copyright (C) 2016-2020 WordPress.org
	 Twenty Seventeen is distributed under the terms of the GNU GPL v2 or later.

	 Twenty Sixteen. Copyright (C) 2015-2020 WordPress.org
	 Twenty Sixteen is distributed under the terms of the GNU GPL v2 or later.

	 Twenty Twenty. Copyright (C) 2020 WordPress.org
	 Twenty Twenty is distributed under the terms of the GNU GPL v2 or later.

	 Underscores https://underscores.me/, Copyright (C) 2012-2020 Automattic, Inc.
	 Underscores is distributed under the terms of the GNU GPL v2 or later.

	 Normalizing styles have been helped along thanks to the fine work of
	 Nicolas Gallagher and Jonathan Neal https://necolas.github.io/normalize.css/

	 Unless otherwise noted, the icons in this theme are derived from the WordPress
	 Icons Library, licensed under the terms of the GNU GPL v2 or later.
	 https://github.com/WordPress/gutenberg/tree/master/packages/icons

	 This theme bundles the following third-party resources:

	 input[type='checkbox'], input[type='radio'], input[type='ranged'],
	 https://css-tricks.com/custom-styling-form-inputs-with-modern-css-features/
https://css-tricks.com/styling-cross-browser-compatible-range-inputs-css/
https://codepen.io/aaroniker/pen/ZEYoxEY Copyright (c) 2020 Aaron Iker
https://codepen.io/chriscoyier/pen/FtnAa Copyright (c) 2020 Chris Coyier
License: MIT.
