module.exports = {
	proxy: 'https://' + process.env.PROXY,
	// XAMPP Apache server SSL config:
	//https: {
	//	key: process.env.XAMPP_DIR + '/certificates/'
	//		+ process.env.PROXY + '.key',
	//	cert: process.env.XAMPP_DIR + '/certificates/'
	//		+ process.env.PROXY + '.crt',
	//},
	// Varying Vagrants Vagrant virtual machine SSL config:
	https: {
		key: process.env.VAGRANT_VVV_DIR + '/certificates/'
			+ process.env.VHOST_NAME + '/dev.key',
		cert: process.env.VAGRANT_VVV_DIR + '/certificates/'
			+ process.env.VHOST_NAME + '/dev.crt',
	},
	files: [
		'**/*.css',
		'**/*.js',
		'**/*.php',
	],
	open: false,
}
